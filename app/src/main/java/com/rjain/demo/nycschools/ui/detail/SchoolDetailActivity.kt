package com.rjain.demo.nycschools.ui.detail

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.rjain.demo.nycschools.Injection
import com.rjain.demo.nycschools.R
import com.rjain.demo.nycschools.databinding.ActivityDetailBinding
import com.rjain.demo.nycschools.model.School
import com.rjain.demo.nycschools.showLongToast
import com.rjain.demo.nycschools.showShortToast

class SchoolDetailActivity : AppCompatActivity() {

    private lateinit var viewModel: SchoolDetailViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setupActionBar()
        val school = intent.getParcelableExtra(INTENT_EXTRA_SCHOOL) as School?
        school?.let {
            title = school.school_name
            val binding: ActivityDetailBinding = DataBindingUtil.setContentView(
                    this, R.layout.activity_detail)
            viewModel = ViewModelProviders.of(this, Injection.provideViewModelFactory(this))
                    .get(SchoolDetailViewModel::class.java)
            viewModel.schoolSatMetrics.observe(this, Observer<List<com.rjain.demo.nycschools.model.SchoolSatMetrics>> {
                //TODO see if we can return just single schoolSatMetric object instead of list
                it?.let {
                    if (it.isNotEmpty()) {
                        val schoolDetail = it[0]
                        binding.schoolSatMetrics = schoolDetail
                        binding.school = school
                    }
                }
            })
            viewModel.errorMessage.observe(this, Observer<String> {
                showLongToast("${getString(R.string.error_message_fetching_data)}$it")
            })
            viewModel.getSchoolSatMetrics(school.dbn)
        } ?: run {
            showShortToast(getString(R.string.something_went_wrong_message))
        }

    }

    private fun setupActionBar() {
        assert(supportActionBar != null)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
    }

    override fun onSupportNavigateUp(): Boolean {
        finish()
        return true
    }

    companion object {
        private const val INTENT_EXTRA_SCHOOL = "school"
        fun newIntent(context: Context, school: School): Intent {
            val intent = Intent(context, SchoolDetailActivity::class.java)
            intent.putExtra(INTENT_EXTRA_SCHOOL, school)
            return intent
        }
    }
}
