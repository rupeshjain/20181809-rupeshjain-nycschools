package com.rjain.demo.nycschools.repository

import androidx.lifecycle.MutableLiveData
import com.rjain.demo.nycschools.data.local.database.SchoolSatMetricLocalCache
import com.rjain.demo.nycschools.data.remote.network.NYCSchoolApi
import com.rjain.demo.nycschools.data.remote.network.SchoolSatMetricResponse
import com.rjain.demo.nycschools.data.remote.network.fetchSchoolDetail

class SchoolSatMetricsRepository(
        private val service: NYCSchoolApi,
        private val cache: SchoolSatMetricLocalCache
) {
    private val networkErrors = MutableLiveData<String>()
    private var isRequestInProgress = false

    fun getSchoolDetails(schoolName: String): SchoolSatMetricResponse {
        fetchAndSaveData(schoolName)
        val data = cache.getSchoolSatMetrics(schoolName)
        return SchoolSatMetricResponse(data, networkErrors)
    }

    private fun fetchAndSaveData(query: String) {
        if (isRequestInProgress) return
        isRequestInProgress = true
        fetchSchoolDetail(service, query, { sat_result ->
            cache.insert(sat_result) {
                isRequestInProgress = false
            }
        }, { error ->
            networkErrors.postValue(error)
            isRequestInProgress = false
        })
    }
}
