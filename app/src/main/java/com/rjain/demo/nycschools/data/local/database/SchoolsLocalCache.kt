package com.rjain.demo.nycschools.data.local.database

import androidx.paging.DataSource
import androidx.sqlite.db.SimpleSQLiteQuery
import com.rjain.demo.nycschools.model.School
import timber.log.Timber
import java.util.concurrent.Executor

/**
 * wrapper class over dao to allow changing the persistent implementation in future if required
 */
class SchoolsLocalCache(
        private val schoolsDao: SchoolsDao,
        private val ioExecutor: Executor
) {
    fun insert(repos: List<School>, insertFinished: () -> Unit) {
        ioExecutor.execute {
            Timber.d("inserting ${repos.size} schools")
            schoolsDao.insert(repos)
            insertFinished()
        }
    }
    fun getSchools(query: String, sortColumn: String): DataSource.Factory<Int, School> {
        return  schoolsDao.getSchools(
                SimpleSQLiteQuery("SELECT * FROM schools WHERE $query ORDER BY $sortColumn ASC"))
    }
}
