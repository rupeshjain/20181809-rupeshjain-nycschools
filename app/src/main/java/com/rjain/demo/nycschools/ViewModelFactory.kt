package com.rjain.demo.nycschools

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.rjain.demo.nycschools.repository.SchoolSatMetricsRepository
import com.rjain.demo.nycschools.repository.SchoolsRepository
import com.rjain.demo.nycschools.ui.detail.SchoolDetailViewModel
import com.rjain.demo.nycschools.ui.list.SchoolsListViewModel

class ViewModelFactory(private val schoolsRepository: SchoolsRepository, private val schoolDetailRepository: SchoolSatMetricsRepository) : ViewModelProvider.Factory {
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(SchoolsListViewModel::class.java)) {
            @Suppress("UNCHECKED_CAST")
            return SchoolsListViewModel(schoolsRepository) as T
        }

        if (modelClass.isAssignableFrom(SchoolDetailViewModel::class.java)) {
            @Suppress("UNCHECKED_CAST")
            return SchoolDetailViewModel(schoolDetailRepository) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}
