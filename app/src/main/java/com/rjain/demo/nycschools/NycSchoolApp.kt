package com.rjain.demo.nycschools
import android.app.Application
import timber.log.Timber.DebugTree
import timber.log.Timber.plant


class NycSchoolApp : Application() {
    override fun onCreate() {
        super.onCreate()
        if (BuildConfig.DEBUG) {
            plant(DebugTree())
        }
    }
}
