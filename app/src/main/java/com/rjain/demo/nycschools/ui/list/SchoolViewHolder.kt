package com.rjain.demo.nycschools.ui.list

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.rjain.demo.nycschools.R
import com.rjain.demo.nycschools.model.School

class SchoolViewHolder(view: View) : RecyclerView.ViewHolder(view) {
    private val txtName: TextView = view.findViewById(R.id.schoolName)
    private val txtLocation: TextView = view.findViewById(R.id.schoolNeighborhood)
    private val txtNeighborhood: TextView = view.findViewById(R.id.schoolNumber)

    fun bind(school: School?, listener: SchoolClickedListener) {
        school?.let {
            showSchoolData(school)
        }
        itemView.setOnClickListener {
            listener.itemClicked(it.tag as School)
        }
    }

    @SuppressLint("SetTextI18n")
    private fun showSchoolData(school: School) {
        txtName.text = school.school_name
        txtNeighborhood.text = school.phone_number
        txtLocation.text = "${school.neighborhood} (${school.borough?.trim()})"
        itemView.tag = school
    }

    companion object {
        fun create(parent: ViewGroup): SchoolViewHolder {
            val view = LayoutInflater.from(parent.context)
                    .inflate(R.layout.school_list_item, parent, false)
            return SchoolViewHolder(view)
        }
    }
}
