package com.rjain.demo.nycschools.model

import android.os.Parcelable
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize


@Entity(tableName = "school_details")
@Parcelize
data class SchoolSatMetrics(
        @PrimaryKey @field:SerializedName("dbn") val dbn: String,
        @field:SerializedName("school_name") val name: String,
        @field:SerializedName("num_of_sat_test_takers") val satTakers: String,
        @field:SerializedName("sat_critical_reading_avg_score") val satCriticalAvgScore: String,
        @field:SerializedName("sat_math_avg_score") val mathAverageScore: String,
        @field:SerializedName("sat_writing_avg_score") val writingAverageScore: String
) : Parcelable

