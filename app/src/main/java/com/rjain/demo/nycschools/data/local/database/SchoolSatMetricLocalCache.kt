package com.rjain.demo.nycschools.data.local.database

import androidx.lifecycle.LiveData
import timber.log.Timber
import java.util.concurrent.Executor
/**
 * wrapper class over dao to allow changing the persistent implementation in future if required
 */
class SchoolSatMetricLocalCache(
        private val schoolsSatDao: SchoolSatMetricsDao,
        private val ioExecutor: Executor
) {
    fun insert(schools: List<com.rjain.demo.nycschools.model.SchoolSatMetrics>, insertFinished: () -> Unit) {
        ioExecutor.execute {
            Timber.d("inserting ${schools.size} schools")
            schoolsSatDao.insert(schools)
            insertFinished()
        }
    }

    fun getSchoolSatMetrics(name: String): LiveData<List<com.rjain.demo.nycschools.model.SchoolSatMetrics>> {
        return schoolsSatDao.getSchoolSatMetrics(name)
    }
}
