package com.rjain.demo.nycschools

object AppConstants {
    const val DEFAULT_COUNT_ROWS_FROM_DB = 25
    const val DEFAULT_COUNT_OF_ITEMS_FROM_SERVER = 100
    const val DEFAULT_LIST_OF_COLUMNS_TO_FETCH = "school_name,dbn,neighborhood,phone_number,overview_paragraph,borough,website,prgdesc1,language_classes,diplomaendorsements"
    const val DEFAULT_ORDERING_COLUMN = "school_name"
}