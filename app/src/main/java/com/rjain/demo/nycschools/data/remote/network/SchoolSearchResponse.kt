package com.rjain.demo.nycschools.data.remote.network

import androidx.lifecycle.LiveData
import androidx.paging.PagedList
import com.rjain.demo.nycschools.model.School

/**
 * SchoolSearchResponse from a getSchoolSatMetrics, which contains LiveData<List<Repo>> holding query data,
 * and a LiveData<String> of network error state.
 */
data class SchoolSearchResponse(
        val data: LiveData<PagedList<School>>,
        val errorMessage: LiveData<String>
)
