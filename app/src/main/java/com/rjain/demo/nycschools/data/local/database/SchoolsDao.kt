package com.rjain.demo.nycschools.data.local.database

import androidx.paging.DataSource
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import com.rjain.demo.nycschools.model.School
import androidx.sqlite.db.SupportSQLiteQuery
import androidx.room.RawQuery



@Dao
interface SchoolsDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(posts: List<School>)

    //since Room doesn't support parameterised ORDER BY clause refer https://stackoverflow.com/questions/44240906/android-room-order-by-not-working
    @RawQuery(observedEntities = [School::class])
    fun getSchools(query: SupportSQLiteQuery): DataSource.Factory<Int, School>
}
