package com.rjain.demo.nycschools.ui.list

import android.os.Bundle
import android.view.KeyEvent
import android.view.View
import android.view.inputmethod.EditorInfo
import android.widget.AdapterView
import android.widget.ArrayAdapter
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.paging.PagedList
import androidx.recyclerview.widget.DividerItemDecoration
import com.rjain.demo.nycschools.Injection
import com.rjain.demo.nycschools.R
import com.rjain.demo.nycschools.model.School
import com.rjain.demo.nycschools.showLongToast
import com.rjain.demo.nycschools.ui.detail.SchoolDetailActivity
import kotlinx.android.synthetic.main.activity_school_list.*

class SchoolListActivity : AppCompatActivity(), SchoolClickedListener, AdapterView.OnItemSelectedListener {
    private lateinit var viewModel: SchoolsListViewModel
    private val adapter = SchoolsListAdapter(this)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_school_list)
        viewModel = ViewModelProviders.of(this, Injection.provideViewModelFactory(this))
                .get(SchoolsListViewModel::class.java)
        setupObservers()
        initList()
        val query = savedInstanceState?.getString(LAST_SEARCH_QUERY) ?: DEFAULT_QUERY
        initSearch(query)
        initSortSpinner()
        viewModel.getSchools(query)

    }

    private fun initList() {
        rvSchoolList.adapter = adapter
        rvSchoolList.itemAnimator = null //To avoid flickering when the data get updated from network
        val decoration = DividerItemDecoration(this, DividerItemDecoration.VERTICAL)
        rvSchoolList.addItemDecoration(decoration)
    }

    private fun initSortSpinner() {
        ArrayAdapter.createFromResource(
                this,
                R.array.sort_columns,
                android.R.layout.simple_spinner_item
        ).also { adapter ->
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            sortSpinner.adapter = adapter
        }
        sortSpinner.isSelected = false
        sortSpinner.setSelection(0,true)
        sortSpinner.onItemSelectedListener = this
    }

    private fun initSearch(query: String) {
        etSearchQuery.setText(query)

        etSearchQuery.setOnEditorActionListener { _, actionId, _ ->
            if (actionId == EditorInfo.IME_ACTION_GO) {
                updateSchoolListBasedOnQuery()
                true
            } else {
                false
            }
        }
        etSearchQuery.setOnKeyListener { _, keyCode, event ->
            if (event.action == KeyEvent.ACTION_DOWN && keyCode == KeyEvent.KEYCODE_ENTER) {
                updateSchoolListBasedOnQuery()
                true
            } else {
                false
            }
        }
    }

    private fun setupObservers() {
        viewModel.schools.observe(this, Observer<PagedList<School>> {
            showEmptyList(it?.size == 0)
            adapter.submitList(it)
        })
        viewModel.errorMessage.observe(this, Observer<String> {
            //TODO discuss should we show persistent notification if the app is in offline mode
            showLongToast("${getString(R.string.error_message_fetching_data)}$it")
        })
    }

    private fun updateSchoolListBasedOnQuery() {
        etSearchQuery.text.let {
            viewModel.getSchools(it.toString(), sortSpinner.selectedItem.toString())
            adapter.submitList(null)
        }
    }

    private fun showEmptyList(show: Boolean) {
        if (show) {
            txtNoData.visibility = View.VISIBLE
            rvSchoolList.visibility = View.GONE
        } else {
            txtNoData.visibility = View.GONE
            rvSchoolList.visibility = View.VISIBLE
        }
    }

    override fun itemClicked(school: School) {
        startActivity(SchoolDetailActivity.newIntent(this, school))
    }

    override fun onNothingSelected(parent: AdapterView<*>?) {
        TODO(" onNothingSelected not implemented")
    }

    override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
        val sortColumn = parent?.getItemAtPosition(position) as String?
        etSearchQuery.text.let {
            sortColumn?.let { sortColumn ->
                viewModel.getSchools(it.toString(), sortColumn)
                adapter.submitList(null)
            }
        }
    }

    companion object {
        private const val LAST_SEARCH_QUERY: String = "last_search_query"
        private const val DEFAULT_QUERY = ""
    }
}
