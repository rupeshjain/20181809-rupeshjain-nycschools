package com.rjain.demo.nycschools.data.remote.network

import com.rjain.demo.nycschools.AppConstants
import com.rjain.demo.nycschools.model.School
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import okhttp3.logging.HttpLoggingInterceptor.Level
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Headers
import retrofit2.http.Query

fun getSchools(
        service: NYCSchoolApi,
        query: String,
        limit: String = AppConstants.DEFAULT_COUNT_OF_ITEMS_FROM_SERVER.toString(),
        offset: Int,
        columns: String = AppConstants.DEFAULT_LIST_OF_COLUMNS_TO_FETCH,
        orderBy: String = AppConstants.DEFAULT_ORDERING_COLUMN,
        onSuccess: (repos: List<School>) -> Unit,
        onError: (error: String) -> Unit
) {
    service.getSchools(columns, limit, offset.toString(), orderBy, query).enqueue(
            object : Callback<List<School>> {
                override fun onFailure(call: Call<List<School>>?, t: Throwable) {
                    onError(t.message ?: "unknown error")
                }

                override fun onResponse(
                        call: Call<List<School>>?,
                        response: Response<List<School>>
                ) {
                    if (response.isSuccessful) {
                        val repos = response.body() ?: emptyList()
                        onSuccess(repos)
                    } else {
                        onError(response.errorBody()?.string() ?: "Unknown error")
                    }
                }
            }
    )
}

fun fetchSchoolDetail(
        service: NYCSchoolApi,
        schoolName: String,
        onSuccess: (repos: List<com.rjain.demo.nycschools.model.SchoolSatMetrics>) -> Unit,
        onError: (error: String) -> Unit
) {
    service.getSchoolSatMetrics(schoolName).enqueue(
            object : Callback<List<com.rjain.demo.nycschools.model.SchoolSatMetrics>> {
                override fun onFailure(call: Call<List<com.rjain.demo.nycschools.model.SchoolSatMetrics>>?, t: Throwable) {
                    onError(t.message ?: "unknown error")
                }

                override fun onResponse(
                        call: Call<List<com.rjain.demo.nycschools.model.SchoolSatMetrics>>?,
                        response: Response<List<com.rjain.demo.nycschools.model.SchoolSatMetrics>>
                ) {
                    if (response.isSuccessful) {
                        val repos = response.body() ?: emptyList()
                        onSuccess(repos)
                    } else {
                        onError(response.errorBody()?.string() ?: "Unknown error")
                    }
                }
            }
    )
}


interface NYCSchoolApi {
    //TODO use querymap to make the API more flexible
    /**
     * Get schools in new york city.
     */
    @Headers("X-SODA2-Legacy-Types: true") //since 2.1 version is case sensitive https://dev.socrata.com/docs/datatypes/text.html#,
    @GET("/resource/s3k6-pzi2.json")
    fun getSchools(
            @Query("\$select") columns: String,
            @Query("\$limit") limit: String,
            @Query("\$offset") offset: String,
            @Query("\$order") order: String,
            @Query("\$where") where: String
    ): Call<List<School>>

    /**
     * Get schools details.
     */
    @GET("/resource/f9bf-2cp4.json")
    fun getSchoolSatMetrics(
            @Query("dbn") schoolName: String
    ): Call<List<com.rjain.demo.nycschools.model.SchoolSatMetrics>>

    companion object {
        private const val BASE_URL = "https://data.cityofnewyork.us"
        fun create(): NYCSchoolApi {
            val logger = HttpLoggingInterceptor()
            logger.level = Level.BASIC
            val client = OkHttpClient.Builder()
                    .addInterceptor(logger)
                    .build()
            return Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .client(client)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build()
                    .create(NYCSchoolApi::class.java)
        }
    }
}