package com.rjain.demo.nycschools.repository

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.paging.PagedList
import com.rjain.demo.nycschools.AppConstants
import com.rjain.demo.nycschools.data.local.database.SchoolsLocalCache
import com.rjain.demo.nycschools.data.remote.network.NYCSchoolApi
import com.rjain.demo.nycschools.data.remote.network.getSchools
import com.rjain.demo.nycschools.model.School

class SchoolBoundaryCallback(
        private val query: String,
        private val sortColumn: String,
        private val service: NYCSchoolApi,
        private val cache: SchoolsLocalCache
) : PagedList.BoundaryCallback<School>() {
    private var lastRequestedPage = 0
    private val _networkErrors = MutableLiveData<String>()
    val networkErrors: LiveData<String>
        get() = _networkErrors
    private var isRequestInProgress = false

    override fun onZeroItemsLoaded() {
        requestAndSaveData(query)
    }

    override fun onItemAtEndLoaded(itemAtEnd: School) {
        requestAndSaveData(query)
    }

    private fun requestAndSaveData(query: String) {
        if (isRequestInProgress) return
        isRequestInProgress = true
        getSchools(service, offset = lastRequestedPage, onSuccess = { repos ->
            cache.insert(repos) {
                lastRequestedPage += AppConstants.DEFAULT_COUNT_OF_ITEMS_FROM_SERVER
                isRequestInProgress = false
            }
        }, onError = { error ->
            _networkErrors.postValue(error)
            isRequestInProgress = false
        }, query = query, orderBy = sortColumn)
    }
}