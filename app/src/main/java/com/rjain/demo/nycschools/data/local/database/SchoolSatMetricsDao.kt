package com.rjain.demo.nycschools.data.local.database

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query

@Dao
interface SchoolSatMetricsDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(posts: List<com.rjain.demo.nycschools.model.SchoolSatMetrics>)

    @Query("SELECT * FROM school_details WHERE (dbn LIKE :queryString)")
    fun getSchoolSatMetrics(queryString: String): LiveData<List<com.rjain.demo.nycschools.model.SchoolSatMetrics>>
}
