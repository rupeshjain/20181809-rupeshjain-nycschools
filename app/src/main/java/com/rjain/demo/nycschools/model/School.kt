package com.rjain.demo.nycschools.model

import android.os.Parcelable
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize


@Entity(tableName = "schools")
@Parcelize
data class School(
        @PrimaryKey @field:SerializedName("dbn") val dbn: String,
        @field:SerializedName("school_name") val school_name: String,//TODO remove _ from variable name
        @field:SerializedName("neighborhood") val neighborhood: String,
        @field:SerializedName("overview_paragraph") val overview: String?,
        @field:SerializedName("diplomaendorsements") val diplomaendorsements: String?,
        @field:SerializedName("website") val website: String?,
        @field:SerializedName("language_classes") val language_classes: String?,
        @field:SerializedName("prgdesc1") val prgdesc1: String?,
        @field:SerializedName("phone_number") val phone_number: String?,
        @field:SerializedName("borough") val borough: String?
) : Parcelable {
    override fun toString(): String {
        return "School(school_name='$school_name', neighborhood='$neighborhood', borough=$borough)"
    }
}

