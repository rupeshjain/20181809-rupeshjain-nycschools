package com.rjain.demo.nycschools.repository

import androidx.paging.LivePagedListBuilder
import com.rjain.demo.nycschools.AppConstants
import com.rjain.demo.nycschools.data.local.database.SchoolsLocalCache
import com.rjain.demo.nycschools.data.remote.network.NYCSchoolApi
import com.rjain.demo.nycschools.data.remote.network.SchoolSearchResponse

class SchoolsRepository(
        private val service: NYCSchoolApi,
        private val cache: SchoolsLocalCache
) {
    fun getSchools(query: String, sortParameter: String): SchoolSearchResponse {
        val dataSourceFactory = cache.getSchools(query,sortParameter)
        val boundaryCallback = SchoolBoundaryCallback(query, sortParameter, service, cache)
        val networkErrors = boundaryCallback.networkErrors
        val data = LivePagedListBuilder(dataSourceFactory, AppConstants.DEFAULT_COUNT_ROWS_FROM_DB)
                .setBoundaryCallback(boundaryCallback)
                .build()
        return SchoolSearchResponse(data, networkErrors)
    }
}
