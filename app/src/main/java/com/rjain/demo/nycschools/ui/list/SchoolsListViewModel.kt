package com.rjain.demo.nycschools.ui.list

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import androidx.lifecycle.ViewModel
import androidx.paging.PagedList
import com.rjain.demo.nycschools.model.School
import com.rjain.demo.nycschools.repository.SchoolsRepository

class SchoolsListViewModel(private val repository: SchoolsRepository) : ViewModel() {

    private val queryLiveData = MutableLiveData<SearchParams>()
    private var schoolSearchResult = Transformations.map(queryLiveData) {
        var query = it.query
        repository.getSchools("school_name like '%$query%' OR neighborhood like '%$query%' OR diplomaendorsements like '%$query%' OR prgdesc1 like '%$query%' OR language_classes like '%$query%'", it.sortColumn)
    }
    val schools: LiveData<PagedList<School>> = Transformations.switchMap(schoolSearchResult) { it.data }
    val errorMessage: LiveData<String> = Transformations.switchMap(schoolSearchResult) {
        it.errorMessage
    }

    fun getSchools(query: String, sortParameter: String = "school_name") {
        queryLiveData.postValue(SearchParams(query, sortParameter))
    }
}

//TBD whether we should use this object in other parts of code too
data class SearchParams(var query: String, var sortColumn: String)


