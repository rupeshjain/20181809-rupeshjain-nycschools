package com.rjain.demo.nycschools.data.local.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.rjain.demo.nycschools.model.School
import com.rjain.demo.nycschools.model.SchoolSatMetrics

@Database(
        entities = [School::class,SchoolSatMetrics::class],
        version = 1,
        exportSchema = false
)
abstract class NycSchoolInfoDatabase : RoomDatabase() {

    abstract fun schoolsDao(): SchoolsDao

    abstract fun schoolSatMetricsDao(): SchoolSatMetricsDao

    companion object {

        @Volatile
        private var INSTANCE: NycSchoolInfoDatabase? = null

        fun getInstance(context: Context): NycSchoolInfoDatabase =
                INSTANCE ?: synchronized(this) {
                    INSTANCE
                            ?: buildDatabase(context).also { INSTANCE = it }
                }

        private fun buildDatabase(context: Context) =
                Room.databaseBuilder(context.applicationContext,
                        NycSchoolInfoDatabase::class.java, "SchoolInfo.db")
                        .build()
    }
}
