package com.rjain.demo.nycschools.ui.detail

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import androidx.lifecycle.ViewModel
import com.rjain.demo.nycschools.repository.SchoolSatMetricsRepository
import com.rjain.demo.nycschools.data.remote.network.SchoolSatMetricResponse

class SchoolDetailViewModel(private val repository: SchoolSatMetricsRepository) : ViewModel() {
    private val queryLiveData = MutableLiveData<String>()
    private val repoResult: LiveData<SchoolSatMetricResponse> = Transformations.map(queryLiveData) {
        repository.getSchoolDetails(it)
    }

    val schoolSatMetrics: LiveData<List<com.rjain.demo.nycschools.model.SchoolSatMetrics>> = Transformations.switchMap(repoResult) { it.data }
    val errorMessage: LiveData<String> = Transformations.switchMap(repoResult) {
        it.errorMessage
    }

    fun getSchoolSatMetrics(queryString: String) {
        queryLiveData.postValue(queryString)
    }
}
