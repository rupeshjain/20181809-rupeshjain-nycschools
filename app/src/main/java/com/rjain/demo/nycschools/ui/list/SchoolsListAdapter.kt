package com.rjain.demo.nycschools.ui.list

import android.view.ViewGroup
import androidx.paging.PagedListAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.rjain.demo.nycschools.model.School


interface SchoolClickedListener {
    fun itemClicked(school: School)
}

class SchoolsListAdapter(private val listener: SchoolClickedListener) : PagedListAdapter<School, RecyclerView.ViewHolder>(SCHOOL_COMPARATOR) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
       return SchoolViewHolder.create(parent)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val schoolItem = getItem(position)
        if (schoolItem != null) {
            (holder as SchoolViewHolder).bind(schoolItem,listener)
        }
    }

    companion object {
        private val SCHOOL_COMPARATOR = object : DiffUtil.ItemCallback<School>() {
            override fun areItemsTheSame(oldItem: School, newItem: School): Boolean =
                    oldItem.dbn == newItem.dbn

            override fun areContentsTheSame(oldItem: School, newItem: School): Boolean =
                    oldItem == newItem
        }
    }
}
