package com.rjain.demo.nycschools.data.remote.network

import androidx.lifecycle.LiveData
import com.rjain.demo.nycschools.model.SchoolSatMetrics

/**
 * SchoolSearchResponse from a getSchoolSatMetrics, which contains LiveData<List<Repo>> holding query data,
 * and a LiveData<String> of network error state.
 */
data class SchoolSatMetricResponse(
        val data: LiveData<List<SchoolSatMetrics>>,
        val errorMessage: LiveData<String>
)
