package com.rjain.demo.nycschools

import android.content.Context
import androidx.lifecycle.ViewModelProvider
import com.rjain.demo.nycschools.data.local.database.NycSchoolInfoDatabase
import com.rjain.demo.nycschools.data.local.database.SchoolSatMetricLocalCache
import com.rjain.demo.nycschools.data.local.database.SchoolsLocalCache
import com.rjain.demo.nycschools.data.remote.network.NYCSchoolApi
import com.rjain.demo.nycschools.repository.SchoolSatMetricsRepository
import com.rjain.demo.nycschools.repository.SchoolsRepository
import java.util.concurrent.Executors

object Injection {

    private fun provideCache(context: Context): SchoolsLocalCache {
        val database = NycSchoolInfoDatabase.getInstance(context)
        return SchoolsLocalCache(database.schoolsDao(), Executors.newSingleThreadExecutor())
    }

    private fun provideSchoolSatMetricsCache(context: Context): SchoolSatMetricLocalCache {
        val database = NycSchoolInfoDatabase.getInstance(context)
        return SchoolSatMetricLocalCache(database.schoolSatMetricsDao(), Executors.newSingleThreadExecutor())
    }

    private fun provideSchoolService(): NYCSchoolApi {
        return NYCSchoolApi.create()
    }

    private fun provideSchoolRepository(context: Context): SchoolsRepository {
        return SchoolsRepository(provideSchoolService(), provideCache(context))
    }

    private fun provideSchoolSatMetricRepository(context: Context): SchoolSatMetricsRepository {
        return SchoolSatMetricsRepository(provideSchoolService(), provideSchoolSatMetricsCache(context))
    }

    fun provideViewModelFactory(context: Context): ViewModelProvider.Factory {
        return ViewModelFactory(provideSchoolRepository(context), provideSchoolSatMetricRepository(context))
    }
}
