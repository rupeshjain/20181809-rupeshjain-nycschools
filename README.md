# NYC High schools information app

The app displays a list of NYC high schools from https://data.cityofnewyork.us/Education/DOE-High-School-Directory-2017/s3k6-pzi2

On tapping on a school item it displays additional information about the school.Currently it displays additional description and sat scores from this link-https://data.cityofnewyork.us/Education/SAT-Results/f9bf-2cp4

User can search/filter result based on school name/courses offered/neighborhood/borough/languages

User can also sort the result based on school name/neighborhood/borough



## High Level Architecture

- The app uses MVVM architecture.
- The app uses paging library to load and display small chunks of data at a time. This is mainly because the dataset is huge. Loading partial data on demand reduces usage of network bandwidth and system resources.
- The app only queries the required subset of dataset columns from the server for efficiency.
- The app stores the data in a database for caching and loading them offline.The app uses Room Apis for this.
- The db acts as single source of truth.The updates to the UI are made by first fetching whatever values are available from the db.A network call is made and the db is updated with new data.UI is updated with the new data.
- The app uses retrofit for making the service call.
- The app uses gson for parsing json objects into pojos.
- The UI consists of two activities - SchoolListActivity for showing list of schools and DetailActivity for showing details about the school.
- The DetailActivity uses data binding for binding views with the data.
- The app uses Timber for logging.


## TODOs
- enhance search functionality with natural language processing using DialogFlow or ML Kit
- Discuss and see if we want to display data in a grid/tableview
- add unit test cases
- since the school info doesn't change often, find more ways to optimize network calls.
- continue polishing the UI and adding more features.

